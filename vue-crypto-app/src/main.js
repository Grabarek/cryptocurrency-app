// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import Vuex from 'vuex'

import App from './App'
import router from './router'
import Vuetify from 'vuetify'
import 'es6-promise/auto'
import 'babel-polyfill'

Vue.use(Vuex)
Vue.use(Vuetify)

Vue.config.productionTip = false;

import store from './store/store'

new Vue({
  el: '#app',
  router,
  store,
  components: { App },
  template: '<App/>'
});


