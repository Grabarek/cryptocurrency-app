import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const state = {
    nightMode: false
};

export default new Vuex.Store({
    state: state,
    mutations: {
        toggleNightMode(state) {
            state.nightMode = !state.nightMode;
        },
    }
})

